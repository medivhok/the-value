
[![npm version](https://img.shields.io/npm/v/the-value.svg)](https://www.npmjs.com/package/the-value)
[![npm downloads](https://img.shields.io/npm/dm/the-value.svg)](https://www.npmjs.com/package/the-value)
[![pipeline status](https://gitlab.com/gregory.verret/the-value/badges/master/pipeline.svg)](https://gitlab.com/gregory.verret/the-value/commits/master)
[![coverage report](https://gitlab.com/gregory.verret/the-value/badges/master/coverage.svg)](https://gitlab.com/gregory.verret/the-value/commits/master)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

# the-value

Provides helper functions to *test if* or *ensure that* a value meets certain criteria.

## Installation

With npm:

```shell
$ npm install the-value
```

With yarn:

```shell
$ yarn add the-value
```


## Helper function list

+ [isAConstructor(value)](#isAConstructor) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
+ [isAnInstanceOf(value, constructorList)](#isAnInstanceOf) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
+ [isANonEmptyObject(value)](#isANonEmptyObject) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
+ [isANonEmptyString(value)](#isANonEmptyString) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
+ [isDefined(value)](#isDefined) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
+ [isNull(value)](#isNull) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
+ [isDefinedAndNonNull(value)](#isDefinedAndNonNull) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
+ [isNotDefined(value)](#isNotDefined) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
+ [mustBeAnInstanceOf(value, Constructor, [errorMessage], [ErrorClass])](#mustBeAnInstanceOf) ⇒ [<code>Object</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)
+ [mustBeANonEmptyObject(value, [errorMessage], [ErrorClass])](#mustBeANonEmptyObject) ⇒ [<code>Object</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)
+ [mustBeDefined(value, [errorMessage], [ErrorClass])](#mustBeDefined) ⇒ <code>\*</code>

* * *


<a name="isAConstructor"></a>

### isAConstructor(value) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
Test if the value is a constructor.

**Kind**: global function  
**Returns**: [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean) - - *true* if the value is a constructor.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to test. |


* * *



<a name="isAnInstanceOf"></a>

### isAnInstanceOf(value, constructorList) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
Test if the value is an instance of a single class or the instance of at least one class in a
list of classes.

**Kind**: global function  
**Returns**: [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean) - True if value is an instance of Constructor.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to test. |
| constructorList | <code>class</code> \| <code>Array.&lt;class&gt;</code> | The Constructor function. |


* * *



<a name="isANonEmptyObject"></a>

### isANonEmptyObject(value) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
Test if the value is a non empty object.

**Kind**: global function  
**Returns**: [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean) - - *true* if the value is a non empty object, and *false* otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to test. |

**Example**  
```js
// Returns false
isANonEmptyObject(null)

// Returns false
isANonEmptyObject({})

// Returns true
isANonEmptyObject({ test: 'test' })
```

* * *



<a name="isANonEmptyString"></a>

### isANonEmptyString(value) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
If the value is a non empty string.

**Kind**: global function  
**Returns**: [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean) - - `true` if the value is a non empty string, and `false` otherwise.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to test. |


* * *



<a name="isDefined"></a>

### isDefined(value) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
Test if the value is not *undefined*.

**Kind**: global function  
**Returns**: [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean) - - True if the value is not *undefined*.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to test. |

**Example**  
```js
// Returns false
isDefined(undefined)

// Returns true
isDefined(null)

// Returns true
isDefined(0)

// Returns true
isDefined('')
```

* * *



<a name="isNull"></a>

### isNull(value) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
Test if the value is *null*.

**Kind**: global function  
**Returns**: [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean) - - True if the value is *null*.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to test. |


* * *



<a name="isDefinedAndNonNull"></a>

### isDefinedAndNonNull(value) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
Test if the value is not *undefined* or *null*.

**Kind**: global function  
**Returns**: [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean) - - True if the value is not undefined and not null.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to test. |

**Example**  
```js
// Returns false
isDefinedAndNonNull(undefined)

// Returns false
isDefinedAndNonNull(null)

// Returns true
isDefinedAndNonNull(0)

// Returns true
isDefinedAndNonNull('')
```

* * *



<a name="isNotDefined"></a>

### isNotDefined(value) ⇒ [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean)
Test if the value is *undefined*.

**Kind**: global function  
**Returns**: [<code>boolean</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Boolean) - - True if the value is *undefined*.  

| Param | Type | Description |
| --- | --- | --- |
| value | <code>\*</code> | The value to test. |


* * *



<a name="mustBeAnInstanceOf"></a>

### mustBeAnInstanceOf(value, Constructor, [errorMessage], [ErrorClass]) ⇒ [<code>Object</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)
Ensure the value is an instance of a certain class.

**Kind**: global function  
**Returns**: [<code>Object</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object) - The value passed to the function.  
**Throws**:

- [<code>TypeError</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/TypeError) Is the value is not a non empty object.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| value | [<code>Object</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object) |  | The value to test. |
| Constructor | <code>class</code> |  | The constructor of the  value. |
| [errorMessage] | [<code>string</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) |  | The error message to give if the test fail. |
| [ErrorClass] | <code>class</code> | <code>TypeError</code> | The error to throw if the test fail. |


* * *



<a name="mustBeANonEmptyObject"></a>

### mustBeANonEmptyObject(value, [errorMessage], [ErrorClass]) ⇒ [<code>Object</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object)
Ensure the value is a non empty object.

**Kind**: global function  
**Returns**: [<code>Object</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object) - The value passed to the function.  
**Throws**:

- [<code>TypeError</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/TypeError) Is the value is not a non empty object.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| value | [<code>Object</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/Object) |  | The value to test. |
| [errorMessage] | [<code>string</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) |  | The error message to give if the test fail. |
| [ErrorClass] | <code>class</code> | <code>TypeError</code> | The error to throw if the test fail. |


* * *



<a name="mustBeDefined"></a>

### mustBeDefined(value, [errorMessage], [ErrorClass]) ⇒ <code>\*</code>
Ensure the value is defined.

**Kind**: global function  
**Returns**: <code>\*</code> - The value passed to the function.  
**Throws**:

- [<code>TypeError</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/TypeError) Is the value is defined.


| Param | Type | Default | Description |
| --- | --- | --- | --- |
| value | <code>\*</code> |  | The value to test. |
| [errorMessage] | [<code>string</code>](https://developer.mozilla.org/docs/Web/JavaScript/Reference/Global_Objects/String) | <code>&quot;The value must be defined!&quot;</code> | The error message if the   test fail. |
| [ErrorClass] | <code>class</code> | <code>TypeError</code> | The type of error to throw if the test fails. |


* * *



## See also

- The [API documentation](/docs/API.md).
- The [tests documentation](/docs/tests.md).


* * *

&copy; 2019 Jean Gregory Verret <gregory.verret@gmail.com>.

