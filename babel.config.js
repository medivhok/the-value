/**
 * @param {Object} api - The babel api.
 */
module.exports = function (api) {
  const options = {
    comments: false,
    minified: true,
    plugins: []
  }

  api.cache(true)

  options.presets = [
    'minify',
    [
      '@babel/preset-env',
      {
        targets: {
          node: 6
        }
      }
    ]
  ]

  options.plugins = []

  options.env = {
    production: {
      sourceMaps: false
    },

    development: {
      sourceMaps: 'inline'
    }
  }

  return options
}
