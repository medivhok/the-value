/* eslint-disable import/no-extraneous-dependencies */
const fs = require('fs-extra')
const jsdoc2md = require('jsdoc-to-markdown')

Promise.all([
  fs.readFile('templates/README.hbs', 'utf8'),
  fs.readFile('templates/API.hbs', 'utf8'),
  fs.readFile('templates/tests.hbs', 'utf8'),
  jsdoc2md.getTemplateData({ files: ['src/**/*.js', 'test/**/*.js'] })
])

  .then(([
    readmeTemplate,
    apiTemplate,
    testsTemplate,
    templateData
  ]) => Promise.all([

    // Create the README.md file.
    jsdoc2md.render({
      data: templateData,
      plugin: 'dmd-globals-docs',
      separators: true,
      template: readmeTemplate
    }).then(output => fs.outputFile('README.md', output, 'utf8')),

    // Create the docs/API.md file.
    jsdoc2md.render({
      data: templateData,
      plugin: 'dmd-globals-docs',
      separators: true,
      template: apiTemplate
    }).then(apiDoc => fs.outputFile('docs/API.md', apiDoc, 'utf8')),

    // Create the docs/tests.md file.
    jsdoc2md.render({
      data: templateData,
      plugin: 'dmd-globals-docs',
      separators: true,
      template: testsTemplate
    }).then(testsDoc => fs.outputFile('docs/tests.md', testsDoc, 'utf8'))
  ]))
