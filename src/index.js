/**
 * Provides helper functions to *test if* or *ensure that* a value meets certain criteria.
 *
 * @module the-value
 * @author Jean Gregory Verret <gregory.verret@gmail.com>
 * @license MIT
 *
 * @borrows isAConstructor
 * @borrows isAnInstanceOf
 * @borrows isANonEmptyObject
 * @borrows isANonEmptyString
 * @borrows isDefined
 * @borrows isNull
 *
 * @borrows isDefinedAndNonNull
 * @borrows isNotDefined
 * @borrows isNotDefinedOrNull
 * @borrows mustBeAnInstanceOf
 * @borrows mustBeANonEmptyObject
 * @borrows mustBeDefined
 */
import * as predicates from './predicates'
import isDefinedAndNonNull from './isDefinedAndNonNull'
import isNotDefined from './isNotDefined'
import isNullOrNotDefined from './isNullOrNotDefined'
import mustBeAnInstanceOf from './mustBeAnInstanceOf'
import mustBeANonEmptyObject from './mustBeANonEmptyObject'
import mustBeANonEmptyString from './mustBeANonEmptyString'
import mustBeDefined from './mustBeDefined'

const theValueModule = {
  ...predicates,
  isDefinedAndNonNull,
  isNotDefined,
  isNullOrNotDefined,
  mustBeAnInstanceOf,
  mustBeANonEmptyObject,
  mustBeANonEmptyString,
  mustBeDefined
}

export const theValue = theValueModule
export default theValueModule
