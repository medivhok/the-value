import { isDefined, isNull } from './predicates'

/**
 * Test if the value is not *undefined* or *null*.
 *
 * @example
 * // Returns false
 * isDefinedAndNonNull(undefined)
 *
 * // Returns false
 * isDefinedAndNonNull(null)
 *
 * // Returns true
 * isDefinedAndNonNull(0)
 *
 * // Returns true
 * isDefinedAndNonNull('')
 *
 * @alias isDefinedAndNonNull
 * @param {*} value - The value to test.
 * @returns {boolean} - True if the value is not undefined and not null.
 */
export default value => (isDefined(value) && !isNull(value))
