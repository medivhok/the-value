/**
 * Test if the value is *undefined*.
 *
 * @alias isNotDefined
 * @param {*} value - The value to test.
 * @returns {boolean} - True if the value is *undefined*.
 */
export default value => (value === undefined)
