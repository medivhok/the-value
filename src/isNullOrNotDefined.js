import { isDefined, isNull } from './predicates'

/**
 * Test if the value is *undefined* or *null*.
 *
 * @alias isNullorNotDefined
 * @param {*} value - The value to test.
 * @returns {boolean} - `true` if the value is `undefined` or `null, and `false` otherwise.
 */
export default value => (isNull(value) || !isDefined(value))
