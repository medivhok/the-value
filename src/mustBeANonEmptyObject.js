import { isANonEmptyObject, isANonEmptyString } from './predicates'

/**
 * Ensure the value is a non empty object.
 *
 * @alias mustBeANonEmptyObject
 * @param {Object} value - The value to test.
 * @param {string} [errorMessage] - The error message to give if the test fail.
 * @param {class} [ErrorClass=TypeError] - The error to throw if the test fail.
 * @returns {Object} The value passed to the function.
 * @throws {TypeError} Is the value is not a non empty object.
 */
export default (value, errorMessage, ErrorClass = TypeError) => {
  const theErrorMessage = isANonEmptyString(errorMessage)
    ? errorMessage
    : 'The value must be a non empty object!'

  if (!isANonEmptyObject(value)) {
    throw new ErrorClass(theErrorMessage)
  }

  return value
}
