import { isANonEmptyString } from './predicates'

/**
 * Ensure the value is a non empty string.
 *
 * @alias mustBeANonEmptyString
 * @param {string} value - The value to test.
 * @param {string} [errorMessage=The value must be a non empty string!] - The error message in
 *   case the test fails.
 * @param {class} [ErrorClass=TypeError] - The type of error to throw in case the test fails.
 * @returns {string} The value passed to the function.
 * @throws {TypeError} Is the value is not a non empty string.
 */
export default (value, errorMessage, ErrorClass = TypeError) => {
  const theErrorMessage = isANonEmptyString(errorMessage)
    ? errorMessage
    : 'The value must be a non empty string!'

  if (!isANonEmptyString(value)) {
    throw new ErrorClass(theErrorMessage)
  }

  return value
}
