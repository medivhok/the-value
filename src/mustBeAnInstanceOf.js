import { isAnInstanceOf, isANonEmptyString } from './predicates'

/**
 * Ensure the value is an instance of a certain class.
 *
 * @alias mustBeAnInstanceOf
 * @param {Object} value - The value to test.
 * @param {class} Constructor - The constructor of the  value.
 * @param {string} [errorMessage] - The error message to give if the test fail.
 * @param {class} [ErrorClass=TypeError] - The error to throw if the test fail.
 * @returns {Object} The value passed to the function.
 * @throws {TypeError} Is the value is not a non empty object.
 */
export default (value, Constructor, errorMessage, ErrorClass) => {
  const theErrorMessage = isANonEmptyString(errorMessage)
    ? errorMessage
    : 'The value must be an instance of!'

  if (!isAnInstanceOf(value, Constructor)) {
    throw new ErrorClass(theErrorMessage)
  }

  return value
}
