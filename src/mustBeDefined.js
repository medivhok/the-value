import { isANonEmptyString, isDefined } from './predicates'

/**
 * Ensure the value is defined.
 *
 * @alias mustBeDefined
 * @param {*} value - The value to test.
 * @param {string} [errorMessage=The value must be defined!] - The error message if the
 *   test fail.
 * @param {class} [ErrorClass=TypeError] - The type of error to throw if the test fails.
 * @returns {*} The value passed to the function.
 * @throws {TypeError} Is the value is defined.
 */
export default (value, errorMessage, ErrorClass = TypeError) => {
  const theErrorMessage = isANonEmptyString(errorMessage)
    ? errorMessage
    : 'The value must be defined!'

  if (!isDefined(value)) {
    throw new ErrorClass(theErrorMessage)
  }

  return value
}
