/**
 * @namespace {Object} predicates
 * @borrows isAConstructor
 * @borrows isAnInstanceOf
 * @borrows isANonEmptyObject
 * @borrows isANonEmptyString
 * @borrows isDefined
 * @borrows isNull
 */

export { default as isAConstructor } from './isAConstructor'
export { default as isAnInstanceOf } from './isAnInstanceOf'
export { default as isANonEmptyObject } from './isANonEmptyObject'
export { default as isANonEmptyString } from './isANonEmptyString'
export { default as isDefined } from './isDefined'
export { default as isNull } from './isNull'
