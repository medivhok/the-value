/**
 * Test if the value is a constructor.
 *
 * @alias isAConstructor
 * @param {*} value - The value to test.
 * @returns {boolean} - *true* if the value is a constructor.
 */
export default (value) => {
  let result = false

  if (value instanceof Function) {
    const { prototype } = value

    result = !!prototype && prototype instanceof Object
  }

  return result
}
