/**
 * Test if the value is a non empty object.
 *
 * @example
 * // Returns false
 * isANonEmptyObject(null)
 *
 * // Returns false
 * isANonEmptyObject({})
 *
 * // Returns true
 * isANonEmptyObject({ test: 'test' })
 *
 * @alias isANonEmptyObject
 * @param {*} value - The value to test.
 * @returns {boolean} - *true* if the value is a non empty object, and *false* otherwise.
 */
export default value => Boolean(
  value &&
  value instanceof Object &&
  Object.keys(value).some(key => Object.prototype.hasOwnProperty.call(value, key))
)
