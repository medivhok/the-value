/**
 * If the value is a non empty string.
 *
 * @alias isANonEmptyString
 * @param {*} value - The value to test.
 * @returns {boolean} - `true` if the value is a non empty string, and `false` otherwise.
 */
export default value => Boolean(value && typeof value === 'string')
