/**
 * Test if the value is an instance of a single class or the instance of at least one class in a
 * list of classes.
 *
 * @alias isAnInstanceOf
 * @param {*} value - The value to test.
 * @param {class|class[]} constructorList - The Constructor function.
 * @returns {boolean} True if value is an instance of Constructor.
 */
export default (value, constructorList) => {
  const list = constructorList instanceof Array ? constructorList : [constructorList]

  return list.some(Constructor => (value instanceof Constructor))
}
