/**
 * Test if the value is not *undefined*.
 *
 * @example
 * // Returns false
 * isDefined(undefined)
 *
 * // Returns true
 * isDefined(null)
 *
 * // Returns true
 * isDefined(0)
 *
 * // Returns true
 * isDefined('')
 *
 * @alias isDefined
 * @param {*} value - The value to test.
 * @returns {boolean} - True if the value is not *undefined*.
 */
export default value => (value !== undefined)
