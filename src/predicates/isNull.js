/**
 * Test if the value is *null*.
 *
 * @alias isNull
 * @param {*} value - The value to test.
 * @returns {boolean} - True if the value is *null*.
 */
export default value => (value === null)
