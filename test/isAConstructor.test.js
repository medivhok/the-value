import assert from 'assert'
import fc from 'fast-check'
import theValue from '../'

describe('theValue.isAConstructor()', () => {
  it('should returns true for a class.', () => {
    class MyTest {}
    assert(theValue.isAConstructor(MyTest))
  })

  it('should returns true for a constructor function.', () => {
    const MyTest = function () { this.test = 'test' }
    assert(theValue.isAConstructor(MyTest))
  })

  it('should returns false for an arrow function.', () => {
    const notAConstructor = () => {}
    assert(theValue.isAConstructor(notAConstructor) === false)
  })

  it('should returns false for all non function types.', () => {
    const everythingExceptFunctions = fc.anything()
      .filter(t => (!(t instanceof Function)))
    fc.assert(
      fc.property(everythingExceptFunctions, value => theValue.isAConstructor(value) === false)
    )
  })
})
