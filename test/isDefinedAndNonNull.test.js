import assert from 'assert'
import fc from 'fast-check'
import theValue from '../'

describe('theValue.isDefinedAndNonNull()', () => {
  it('should returns true for everything except \'undefined\' or \'null\'.', () => {
    fc.assert(
      fc.property(
        fc.anything().filter(t => (t !== undefined) && (t !== null)),
        theValue.isDefinedAndNonNull
      )
    )
  })

  it('should returns false for \'null\'.', () => {
    assert(theValue.isDefinedAndNonNull(null) === false)
  })

  it('should returns false for \'undefined\'.', () => {
    assert(theValue.isDefinedAndNonNull(undefined) === false)
  })
})
