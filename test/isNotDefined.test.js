import assert from 'assert'
import fc from 'fast-check'
import theValue from '../'

describe('theValue.isNotDefined()', () => {
  it('should returns false for everything except \'undefined\'.', () => {
    fc.assert(
      fc.property(
        fc.anything().filter(t => t !== undefined),
        value => theValue.isNotDefined(value) === false
      )
    )
  })

  it('should returns false for \'null\'.', () => {
    assert(theValue.isDefined(null))
  })

  it('should returns true for \'undefined\'.', () => {
    assert(theValue.isDefined(undefined) === false)
  })
})
