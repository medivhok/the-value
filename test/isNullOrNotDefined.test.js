import assert from 'assert'
import fc from 'fast-check'
import theValue from '../'

describe('theValue.isNullOrNotDefined()', () => {
  it('should returns false for everything except \'undefined\' or \'null\'.', () => {
    fc.assert(
      fc.property(
        fc.anything().filter(t => (t !== undefined) && (t !== null)),
        value => theValue.isNullOrNotDefined(value) === false
      )
    )
  })

  it('should returns true for \'null\'.', () => {
    assert(theValue.isNullOrNotDefined(null))
  })

  it('should returns true for \'undefined\'.', () => {
    assert(theValue.isNullOrNotDefined(undefined))
  })
})
