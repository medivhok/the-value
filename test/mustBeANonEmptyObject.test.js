import assert from 'assert'
import fc from 'fast-check'
import theValue from '../'

describe('theValue.mustBeANonEmptyObject()', () => {
  it('should returns the object for non empty objects.', () => {
    const nonEmptyObjects = fc.object().map((value) => {
      const valueRef = value
      valueRef.propertyForTesting = 'test'
      return valueRef
    })

    fc.assert(
      fc.property(
        nonEmptyObjects,
        value => theValue.mustBeANonEmptyObject(value) === value
      )
    )
  })

  it('should throws for non objects.', () => {
    const allExceptNonEmptyObjects = fc.anything().filter(t => (typeof t !== 'object'))

    fc.assert(
      fc.property(
        allExceptNonEmptyObjects,
        (value) => {
          assert.throws(() => theValue.mustBeANonEmptyObject(value))
          return true
        }
      )
    )
  })

  it('should throws for an empty object.', () => {
    assert.throws(() => theValue.mustBeANonEmptyObject({}))
  })

  it('should throws for null.', () => {
    assert.throws(() => theValue.mustBeANonEmptyObject(null))
  })
})
