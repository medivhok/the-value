import assert from 'assert'
import fc from 'fast-check'
import theValue from '../'

describe('theValue.mustBeANonEmptyString()', () => {
  it('should returns the string for non empty strings.', () => {
    const nonEmptyStrings = fc.string(1, 150)
    fc.assert(
      fc.property(
        nonEmptyStrings,
        value => theValue.mustBeANonEmptyString(value) === value
      )
    )
  })

  it('should throws for non strings.', () => {
    const everythingExceptNonEmptyStrings = fc.anything()
      .filter(t => (typeof t !== 'string'))

    fc.assert(
      fc.property(
        everythingExceptNonEmptyStrings,
        (value) => {
          assert.throws(() => theValue.mustBeANonEmptyString(value))
          return true
        }
      )
    )
  })

  it('should throws for an empty string.', () => {
    assert.throws(() => theValue.mustBeANonEmptyString(''))
  })
})
