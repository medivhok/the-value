import assert from 'assert'
import fc from 'fast-check'
import theValue from '../'

describe('theValue.mustBeAnInstanceOf()', () => {
  class MyTest {}

  it('should not throws and returns the instance if the test pass.', () => {
    const myTest = new MyTest()
    assert.doesNotThrow(() => theValue.mustBeAnInstanceOf(myTest, MyTest))
    assert(theValue.mustBeAnInstanceOf(myTest, MyTest) === myTest)
  })

  it('should not throws and returns the instance of a class in an array.', () => {
    const myTest = new MyTest()
    assert.doesNotThrow(() => theValue.mustBeAnInstanceOf(myTest, [MyTest]))
    assert(theValue.mustBeAnInstanceOf(myTest, [MyTest]) === myTest)
  })

  it('should throws for non instance of a class.', () => {
    fc.assert(
      fc.property(
        fc.anything(),
        (value) => {
          assert.throws(() => theValue.mustBeAnInstanceOf(value, MyTest))
          return true
        }
      )
    )
  })

  it('should throws for non instance of a class in an array.', () => {
    fc.assert(
      fc.property(
        fc.anything(),
        (value) => {
          assert.throws(() => theValue.mustBeAnInstanceOf(value, [MyTest]))
          return true
        }
      )
    )
  })
})
