import assert from 'assert'
import fc from 'fast-check'
import theValue from '../'

describe('theValue.mustBeDefined()', () => {
  it('should not throws and return the value for everything except \'undefined\'.', () => {
    fc.assert(
      fc.property(
        fc.anything().filter(t => ((t !== undefined) && !Number.isNaN(t))),
        (value) => {
          assert.doesNotThrow(() => theValue.mustBeDefined(value))
          return (theValue.mustBeDefined(value) === value)
        }
      )
    )
  })

  it('should not throws and return the value for \'null\'.', () => {
    assert.doesNotThrow(() => theValue.mustBeDefined(null))
    assert(theValue.mustBeDefined(null) === null)
  })

  it('should throws for \'undefined\'.', () => {
    assert.throws(() => theValue.mustBeDefined(undefined))
  })
})
